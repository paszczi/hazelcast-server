package com.gft.hazelcastserver.configuration;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HazelcastConfiguration {
    private String devHazelcastName = "DEV";
    private String ftHazelcastName = "FT";

    @Bean(destroyMethod = "shutdown")
    public HazelcastInstance devHazelcastInstance() {
        Config config = new Config();
        config.setInstanceName(devHazelcastName);
        config.getGroupConfig().setName(devHazelcastName);
        HazelcastInstance instance = Hazelcast.newHazelcastInstance(config);
        return instance;
    }

    @Bean(destroyMethod = "shutdown")
    public HazelcastInstance ftHazelcastInstance() {
        Config config = new Config();
        config.setInstanceName(ftHazelcastName);
        config.getGroupConfig().setName(ftHazelcastName);
        HazelcastInstance instance = Hazelcast.newHazelcastInstance(config);
        return instance;
    }
}
